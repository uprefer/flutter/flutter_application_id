## 2.0.0

- Dart 3 compatibility
- Gradles syntax support improved (properties "xxxx = yyyy" are now supported)

## 2.0.0-dev

- Nullsafety

## 1.0.0

- Update dependencies
- Add usage examples
- Improve Readme

## 0.0.6

- Updates Application name if requested

## 0.0.5

- Updates Pbxproj instead of Plist.

## 0.0.4

- Actually updates project conf files :)

## 0.0.3

- Fixed Readme
- Better sources formatting

## 0.0.2

- Updating meta data to increase scoring :)

## 0.0.1

- Initial version