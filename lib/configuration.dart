import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter_application_id/custom_exceptions.dart';
import 'package:yaml/yaml.dart';

class PlatformConfiguration extends Equatable {
  const PlatformConfiguration({
    this.id,
    this.name,
  });

  static PlatformConfiguration? fromYamlMap(dynamic map) {
    if (map == null) {
      return null;
    }
    if (map is! YamlMap) {
      throw InvalidFormatException();
    }
    return PlatformConfiguration(id: map[_idKey], name: map[_nameKey]);
  }

  static const String _idKey = 'id';
  static const String _nameKey = 'name';

  final String? id;
  final String? name;

  @override
  List<Object?> get props => [id, name];
}

class Configuration extends Equatable {
  const Configuration({
    this.android,
    this.ios,
  });
  factory Configuration.fromString(String data) {
    final yamlMap = _loadYaml(data);

    if (!yamlMap.containsKey(_flutterApplicationIdKey) ||
        yamlMap[_flutterApplicationIdKey] is! YamlMap) {
      throw NoConfigFoundException();
    }
    return Configuration(
      android: PlatformConfiguration.fromYamlMap(
        yamlMap[_flutterApplicationIdKey][_androidKey],
      ),
      ios: PlatformConfiguration.fromYamlMap(
        yamlMap[_flutterApplicationIdKey][_iosKey],
      ),
    );
  }

  static const String _flutterApplicationIdKey = 'flutter_application_id';
  static const String _iosKey = 'ios';
  static const String _androidKey = 'android';
  final PlatformConfiguration? android;
  final PlatformConfiguration? ios;

  static YamlMap _loadYaml(String data) {
    try {
      final dynamic loadedObject = loadYaml(data);

      if (loadedObject is! YamlMap) throw InvalidFormatException();

      return loadedObject;
    } catch (e) {
      throw InvalidFormatException();
    }
  }

  static Future<Configuration> fromFile(File file) async {
    return Configuration.fromString(await file.readAsString());
  }

  @override
  List<Object?> get props => [android, ios];
}
