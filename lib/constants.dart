const List<String> defaultConfigFiles = <String>[
  'flutter_application_id.yaml',
  'pubspec.yaml',
];

const String androidGradleFile = 'android/app/build.gradle';
const String androidManifestFile = 'android/app/src/main/AndroidManifest.xml';
const String androidAppIdKey = 'applicationId';
const String androidAppnameKey = 'android:label';

const String iosPbxprojFile = 'ios/Runner.xcodeproj/project.pbxproj';
const String iosPlistFile = 'ios/Runner/Info.plist';
const String iosAppidKey = 'PRODUCT_BUNDLE_IDENTIFIER';
const String iosAppnameKey = 'CFBundleName';
