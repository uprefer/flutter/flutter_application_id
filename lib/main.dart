import 'dart:async';
import 'dart:io';

import 'package:args/args.dart';
import 'package:flutter_application_id/configuration.dart';
import 'package:flutter_application_id/constants.dart';
import 'package:flutter_application_id/custom_exceptions.dart';
import 'package:flutter_application_id/file_updater/file_updater.dart';
import 'package:flutter_application_id/file_updater/rules/gradle.dart';
import 'package:flutter_application_id/file_updater/rules/pbxproj.dart';
import 'package:flutter_application_id/file_updater/rules/plist.dart';
import 'package:flutter_application_id/file_updater/rules/xml.dart';

const String fileOption = 'file';
const String helpFlag = 'help';

Future<void> updateApplicationIdFromArguments(List<String> arguments) async {
  // Make default null to differentiate when it is explicitly set
  final parser = ArgParser()
    ..addFlag(
      helpFlag,
      abbr: 'h',
      help: 'Usage help',
      negatable: false,
    )
    ..addOption(
      fileOption,
      abbr: 'f',
      help: 'Config file (default: $defaultConfigFiles)',
    );
  final argResults = parser.parse(arguments);

  if (argResults[helpFlag]) {
    stdout
      ..writeln('Updates application id for iOS and Android')
      ..writeln(parser.usage);
    exit(0);
  }

  try {
    final config = await loadConfigFileFromArgResults(
      argResults,
      verbose: true,
    );

    await updateApplicationIdFromConfig(config);
  } catch (e) {
    if (e is InvalidFormatException) {
      stderr.writeln('Invalid configuration format.');
    } else {
      stderr.writeln(e);
    }
    exit(2);
  }
}

Future<void> updateAndroidApplicationIdFromConfig(Configuration config) async {
  if (config.android == null) return;

  if (config.android!.id != null) {
    stdout.writeln('Updating Android application Id');
    await FileUpdater.updateFile(
      File(androidGradleFile),
      GradleString(
        androidAppIdKey,
        config.android!.id!,
      ),
    );
  }
  if (config.android!.name != null) {
    stdout.writeln('Updating Android application name');
    await FileUpdater.updateFile(
      File(androidManifestFile),
      XmlAttribute(
        androidAppnameKey,
        config.android!.name!,
      ),
    );
  }
}

Future<void> updateIosApplicationIdFromConfig(Configuration config) async {
  if (config.ios == null) return;

  if (config.ios!.id != null) {
    stdout.writeln('Updating iOS application Id');
    await FileUpdater.updateFile(
      File(iosPbxprojFile),
      Pbxproj(
        iosAppidKey,
        config.ios!.id!,
      ),
    );
  }
  if (config.ios!.name != null) {
    stdout.writeln('Updating iOS application name');
    await FileUpdater.updateFile(
      File(iosPlistFile),
      Plist(
        iosAppnameKey,
        config.ios!.name!,
      ),
    );
  }
}

Future<void> updateApplicationIdFromConfig(Configuration config) async {
  await updateAndroidApplicationIdFromConfig(config);
  await updateIosApplicationIdFromConfig(config);
}

Future<Configuration> loadConfigFileFromArgResults(
  ArgResults argResults, {
  bool verbose = false,
}) async {
  final config = await loadConfigFile(
    filePath: argResults[fileOption],
    verbose: verbose,
  );
  if (config != null) return config;

  for (final configFile in defaultConfigFiles) {
    final defaultConfig = await loadConfigFile(
      filePath: configFile,
      verbose: verbose,
    );
    if (defaultConfig != null) return defaultConfig;
  }

  throw NoConfigFoundException();
}

Future<Configuration?> loadConfigFile({
  String? filePath,
  bool verbose = false,
}) async {
  if (filePath == null) return null;

  try {
    return Configuration.fromFile(File(filePath));
  } catch (e) {
    if (verbose) {
      stderr.writeln(e);
    }
    return null;
  }
}
