import 'dart:io';

class UpdateRule {
  String update(String line) => line;
}

class FileUpdater {
  FileUpdater(List<String> lines) : _data = lines;
  factory FileUpdater.fromString(String s) {
    return FileUpdater(s.split('\n'));
  }

  final List<String> _data;

  static Future<void> updateFile(File file, UpdateRule updateRule) async {
    final fileUpdater = await FileUpdater.fromFile(file);
    fileUpdater.update(updateRule);
    await fileUpdater.toFile(file);
  }

  static Future<FileUpdater> fromFile(File file) async {
    return FileUpdater(await file.readAsLines());
  }

  Future<void> toFile(File file) async {
    await file.writeAsString(_data.join('\n'));
  }

  void update(UpdateRule rule) {
    for (var x = 0; x < _data.length; x++) {
      _data[x] = rule.update(_data[x]);
    }
  }

  @override
  String toString() {
    return _data.join('\n');
  }
}
